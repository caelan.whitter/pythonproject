# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 15:00:39 2021

@author: Caelan & mikyb
"""
from urllib.request import Request, urlopen

from bs4 import BeautifulSoup as BS

def get_html(url):
    req = Request (url ,headers={'user-agent': 'Mozilla/5.0'})
    html_code = urlopen(req)
    return html_code.read()

def save_to_local_file(filename,html_bytes):
    try:

        fobj = open("local_html/"+filename,"wb")
        fobj.write(html_bytes)
        
    except:
        print('error')
        
    finally:
        fobj.close()
        
def test_bs(html_bytes):
    return BS (html_bytes)




