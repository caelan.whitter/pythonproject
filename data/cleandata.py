# -*- coding: utf-8 -*-
"""
Created on Fri Mar 19 16:28:23 2021

@author: Caelan
"""

class CleanData:
    def __init__(self,bs_obj,day):
        self.bs_obj = bs_obj
        self.day = day
    
    def seperate_cat(self):
        new_obj = self.bs_obj.find(id='main_table_countries_'+self.day)
        content = new_obj.find_all('td')
        content = content[152:4351]
        rank = ""
        country=[]
        total_cases = []
        new_cases = []
        total_deaths = []
        new_deaths = []
        total_recovered = []
        new_recovered = []
        active_cases = []
        serious_critical = []
        totcases_1m = []
        deaths_1m = []
        total_tests = []
        tests_1m = []
        population = []
        continent = []
        case_everyx = []
        death_everyx = []
        test_everyx = []
        i =1
        for entries in content:
            
            if i%19 == 1:
                rank = rank+(', '+entries.text.strip())
                
            if i%19 == 2:
                country.append(entries.text.strip())
                
            if i%19 == 3:
               
                total_cases.append(self.good_numbers(entries.text.strip()))
                
                
            if i%19 == 4:
                new_cases.append(self.good_numbers(entries.text.strip()))
                
            if i%19 == 5:
                total_deaths.append(self.good_numbers(entries.text.strip()))
                
            if i%19 == 6:
                new_deaths.append(self.good_numbers( entries.text.strip()))
                
            if i%19 == 7:
                total_recovered.append(self.good_numbers(entries.text.strip()))
                
            if i%19 == 8:
                new_recovered.append(self.good_numbers(entries.text.strip()))
                
            if i%19 == 9:
                active_cases.append(self.good_numbers(entries.text.strip()))
                
            if i%19 == 10:
                serious_critical.append(self.good_numbers(entries.text.strip()))
                
            if i%19 == 11:
                totcases_1m.append(self.good_numbers(entries.text.strip()))
                
            if i%19 == 12:
                deaths_1m.append(self.good_numbers(entries.text.strip()))
                
            if i%19 == 13:
                total_tests.append(self.good_numbers(entries.text.strip()))
                
            if i%19 == 14:
                tests_1m.append(self.good_numbers(entries.text.strip()))
                
            if i%19 == 15:
                population.append(self.good_numbers(entries.text.strip()))
                
            if i%19 == 16:
                continent.append(entries.text.strip())
                
            if i%19 == 17:
                case_everyx.append(self.good_numbers(entries.text.strip()))
                
            if i%19 == 18:
                death_everyx.append(self.good_numbers(entries.text.strip()))
                
            if i%19 == 0:
                test_everyx.append(self.good_numbers(entries.text.strip()))
            i+=1
            
        rank = rank[2:]
        words2 = []
        for words in rank.split(', '):
            word = []
            word.append(words)
            words2.append(word)
        rank = words2
        final_list = self.final_list(rank,country,total_cases,new_cases,total_deaths,new_deaths,total_recovered,new_recovered,active_cases,serious_critical,totcases_1m,deaths_1m,total_tests,tests_1m,population,continent,case_everyx,death_everyx,test_everyx)
        return final_list

    
    def good_numbers(self,entries):
                if entries == '' or entries == 'N/A':
                    return 0
                else:
                    if '+' in entries:
                        without_plus = entries.split('+')
                        if ',' in without_plus[1]:
                        
                            without_coma = without_plus[1].split(',')
                            together = without_coma[0]+without_coma[1]
                            return int(together)
                        else:
                            return int(without_plus[1])
                    elif ',' in entries:
                        without_coma = entries.split(',')
                        if len(without_coma) == 2:
                                together = without_coma[0]+without_coma[1]
                                return int(together)
                        else:
                                together = without_coma[0]+without_coma[1]+without_coma[2]
                                return int(together)
                    elif '.' in entries:
                        return float(entries)
                    else:
    
                        return int(entries)
                    
    def final_list(self,rank,country,total_cases,new_cases,total_deaths,new_deaths,total_recovered,new_recovered,active_cases,serious_critical,totcases_1m,deaths_1m,total_tests,tests_1m,population,continent,case_everyx,death_everyx,test_everyx):
        self.adding_to_rank(rank,country)
        self.adding_to_rank(rank,total_cases)
        self.adding_to_rank(rank,new_cases)
        self.adding_to_rank(rank,total_deaths)
        self.adding_to_rank(rank,new_deaths)
        self.adding_to_rank(rank,total_recovered)
        self.adding_to_rank(rank,new_recovered)
        self.adding_to_rank(rank,active_cases)
        self.adding_to_rank(rank,serious_critical)
        self.adding_to_rank(rank,totcases_1m)
        self.adding_to_rank(rank,deaths_1m)
        self.adding_to_rank(rank,total_tests)
        self.adding_to_rank(rank,tests_1m)
        self.adding_to_rank(rank,population)
        self.adding_to_rank(rank,continent)
        self.adding_to_rank(rank,case_everyx)
        self.adding_to_rank(rank,death_everyx)
        self.adding_to_rank(rank,test_everyx)
        return rank
    
    def adding_to_rank(self,rank,adding):
        i=0
        for words in adding:
            rank[i].append(words)
            i+=1
        return rank
    

    
    
    