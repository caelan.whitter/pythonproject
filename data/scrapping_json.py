# -*- coding: utf-8 -*-
"""
Created on Thu Mar 25 21:49:17 2021

@author: Caelan & mikyb
"""

import json
def json_info():

    with open('countries_json/country_neighbour_dist_file.json') as file:

        data = json.load(file)   
    
  
    i = 0

    country_list_final=[]
    for lines in data:
        for k in lines.keys():
            
            for k2 in data[i][k].keys():
                country_list=[]

                country_list.append(k)
                country_list.append(k2)
                country_list.append(data[i][k][k2])
            
               

                country_list_final.append(country_list)
        i+=1
    j=1
    for countries in country_list_final:
        countries.insert(0,j)
        j+=1



    return country_list_final


#print(json_info())