# -*- coding: utf-8 -*-
"""
Created on Fri Mar 19 17:49:00 2021

@author: Caelan
"""
from mysql.connector import connect, Error
from data.cleandata import CleanData
from data.scrapping import *
from data.scrapping_json import *

def connect_db(db):
    try:
        if db is not None:
            return connect(
                host="localhost",
                user="root",
                password="",
                database=db)
        else:
            return connect(
                host="localhost",
                user="root",
                password="",
                )
    except Error as err:
        print(err)

def create_db_covid(db):
    try:
        connec = connect_db(None)
        query1 = 'CREATE DATABASE {0}'.format(db)
        connec.cursor().execute(query1)
    except Error as err:
        print(err)

def select_db(db):
    try:
        connec = connect_db(None)
        query1= 'USE {0}'.format(db)
        query2= 'SHOW TABLES'
        connec.cursor().execute(query1)
        connec.cursor().execute(query2)
        result = connec.cursor().fetchall()
        for lines in result:
            print(lines)
    except Error as err:
        print(err)

def create_table(db,table_name,table_schema):
    try:
        connec = connect_db(db)
        query1 = "DROP TABLE IF EXISTS {0}".format(table_name)
        connec.cursor().execute(query1)
        connec.commit()
    finally:
        query2 = "CREATE TABLE {0}{1}".format(table_name,table_schema)
        connec.cursor().execute(query2)
        connec.commit()
        

def select_data_from_table(db,table_name,columns_str,criteria_str):
    connec = connect_db(db)
    query1="SELECT {0} FROM {1} WHERE {2}".format(columns_str,table_name,criteria_str)
    connec.cursor().execute(query1)
    connec.cursor().fetchall()


def populate_table(db,table_name,table_schema,list_tuples):
    connec = connect_db(db)
    values = (','.join(['%s']*(len(list_tuples[0]))))

    query1 = "INSERT INTO {0} VALUES ({1})".format(table_name,values)


    connec.cursor().executemany(query1,list_tuples)

    connec.commit()
    
def create_db_tables_six_days():
    
    table_schema = "(ranks int(30) NOT NULL, \
                     Country_Other varchar(55) NOT NULL, \
                     TotalCases int(30) NOT NULL, \
                     NewCases int(30) NOT NULL, \
                     TotalDeaths int(30) NOT NULL, \
                     NewDeaths int(30) NOT NULL, \
                     TotalRecovered int(30) NOT NULL, \
                     NewRecovered int(30) NOT NULL, \
                     ActiveCases int(30) NOT NULL, \
                     Serious_Critical int(30) NOT NULL, \
                     `Tot Cases/1m pop` varchar(50) NOT NULL, \
                     `Deaths/1M` int(30) NOT NULL, \
                     TotalTests int(30) NOT NULL, \
                     `Tests/1M pop` decimal(10,2), \
                     Population int(30) NOT NULL, \
                     Continent varchar(55) NOT NULL, \
                     `1 Caseevery X ppl` decimal(10,2), \
                     `1 Deathevery X ppl` decimal(10,2), \
                     `1 Testevery X ppl` decimal(10,2), \
                     PRIMARY KEY (Country_Other))"
    
    create_db_covid('covid_corona_db_WHIT_BARI')
    db='covid_corona_db_WHIT_BARI'
    connect_db(db)
    url='file:///C:/Users/Caelan/Documents/fourthsemester/python/pythonproject/local_html/local_page2021-03-18.html'
    html_bytes = get_html(url)
    bs_obj = test_bs(html_bytes)
    
    first_date_table = "t2021_03_16"  
    create_table(db,first_date_table,table_schema)
    final_obj = CleanData(bs_obj,'yesterday2')
    clean_obj = final_obj.seperate_cat()
    populate_table(db,first_date_table,table_schema,clean_obj)
    
    second_date_table = "t2021_03_17" 
    create_table(db,second_date_table,table_schema)
    final_obj = CleanData(bs_obj,'yesterday')
    clean_obj = final_obj.seperate_cat()
    populate_table(db,second_date_table,table_schema,clean_obj)
    
    third_date_table = "t2021_03_18"  
    create_table(db,third_date_table,table_schema)
    final_obj = CleanData(bs_obj,'today')
    clean_obj = final_obj.seperate_cat()
    populate_table(db,third_date_table,table_schema,clean_obj)


    url='file:///C:/Users/Caelan/Documents/fourthsemester/python/pythonproject/local_html/local_page2021-03-21.html'


    html_bytes = get_html(url)
    bs_obj = test_bs(html_bytes)
    
    fourth_date_table = "t2021_03_19"
    create_table(db,fourth_date_table,table_schema)
    final_obj = CleanData(bs_obj,'yesterday2')
    clean_obj = final_obj.seperate_cat()
    populate_table(db,fourth_date_table,table_schema,clean_obj)
    
    fifth_date_table = "t2021_03_20" 
    create_table(db,fifth_date_table,table_schema)
    final_obj = CleanData(bs_obj,'yesterday')
    clean_obj = final_obj.seperate_cat()
    populate_table(db,fifth_date_table,table_schema,clean_obj)
    
    sixth_date_table = "t2021_03_21"   
    create_table(db,sixth_date_table,table_schema)
    final_obj = CleanData(bs_obj,'today')
    clean_obj = final_obj.seperate_cat()
    populate_table(db,sixth_date_table,table_schema,clean_obj)
    
def create_border_table():

    db='covid_corona_db_WHIT_BARI'
    connect_db(db)
    table_schema = "(ranks int(10) NOT NULL,\
                     Country_Other varchar(60) NOT NULL, \
                     Neighbour varchar(60) NOT NULL, \
                     Distance decimal(20,2) NOT NULL, \
                     PRIMARY KEY (ranks))"
    create_table(db,'country_borders_table',table_schema)

    table_data = json_info()
   
    populate_table(db, 'country_borders_table', table_schema, table_data)
    
def creating_new_table(url,table_name,add_date):
    db='covid_corona_db_WHIT_BARI'
    add_date_split = add_date.split('-')
    dates_split = table_name.split('_')
    dates = int(dates_split[2])
    adding_dates = int(add_date_split[2])
    table_schema = "(ranks int(30) NOT NULL, \
                     Country_Other varchar(55) NOT NULL, \
                     TotalCases int(30) NOT NULL, \
                     NewCases int(30) NOT NULL, \
                     TotalDeaths int(30) NOT NULL, \
                     NewDeaths int(30) NOT NULL, \
                     TotalRecovered int(30) NOT NULL, \
                     NewRecovered int(30) NOT NULL, \
                     ActiveCases int(30) NOT NULL, \
                     Serious_Critical int(30) NOT NULL, \
                     `Tot Cases/1m pop` varchar(50) NOT NULL, \
                     `Deaths/1M` int(30) NOT NULL, \
                     TotalTests int(30) NOT NULL, \
                     `Tests/1M pop` decimal(10,2), \
                     Population int(30) NOT NULL, \
                     Continent varchar(55) NOT NULL, \
                     `1 Caseevery X ppl` decimal(10,2), \
                     `1 Deathevery X ppl` decimal(10,2), \
                     `1 Testevery X ppl` decimal(10,2), \
                     PRIMARY KEY (Country_Other))"
 
    html_bytes = get_html(url)
    bs_obj = test_bs(html_bytes)

    first_table = dates_split[0]+'_'+dates_split[1]+'_'+str(dates-2)
    create_table(db,first_table,table_schema)
    final_obj = CleanData(bs_obj,'yesterday2')
    clean_obj = final_obj.seperate_cat()
    populate_table(db,first_table,table_schema,clean_obj)
    yes2_date = add_date_split[0]+'-'+add_date_split[1]+'-'+str(adding_dates-2)
    adding_date_new_tables(first_table,yes2_date)
    

    
    second_table = dates_split[0]+'_'+dates_split[1]+'_'+str(dates-1)
    create_table(db,second_table,table_schema)
    final_obj = CleanData(bs_obj,'yesterday')
    clean_obj = final_obj.seperate_cat()
    populate_table(db,second_table,table_schema,clean_obj)
    yes_date = add_date_split[0]+'-'+add_date_split[1]+'-'+str(adding_dates-1)
    adding_date_new_tables(second_table,yes_date)
    

    third_table = table_name   
    create_table(db,third_table,table_schema)
    final_obj = CleanData(bs_obj,'today')
    clean_obj = final_obj.seperate_cat()
    populate_table(db,third_table,table_schema,clean_obj)
    today_date = add_date_split[0]+'-'+add_date_split[1]+'-'+str(adding_dates)
    adding_date_new_tables(third_table,today_date)
    
    list_first_tables = [first_table,second_table,third_table]
    return list_first_tables
    
def adding_date_new_tables(names,dates):

    db='covid_corona_db_WHIT_BARI'
    connec=connect_db(db)
    query1 = "ALTER TABLE "+names+" ADD dates date DEFAULT '"+dates+"'"


    connec.cursor().execute(query1)
    connec.commit()

    
def adding_date_old_tables():
    db='covid_corona_db_WHIT_BARI'
    connec=connect_db(db)
    query1 = "ALTER TABLE t2021_03_16 ADD dates date DEFAULT '2021-03-16'"
    query2 = "ALTER TABLE t2021_03_17 ADD dates date DEFAULT '2021-03-17'"
    query3 = "ALTER TABLE t2021_03_18 ADD dates date DEFAULT '2021-03-18'"
    query4 = "ALTER TABLE t2021_03_19 ADD dates date DEFAULT '2021-03-19'"
    query5 = "ALTER TABLE t2021_03_20 ADD dates date DEFAULT '2021-03-20'"
    query6 = "ALTER TABLE t2021_03_21 ADD dates date DEFAULT '2021-03-21'"
    connec.cursor().execute(query1)
    connec.cursor().execute(query2)
    connec.cursor().execute(query3)
    connec.cursor().execute(query4)
    connec.cursor().execute(query5)
    connec.cursor().execute(query6)
    connec.commit()

