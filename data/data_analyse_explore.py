# -*- coding: utf-8 -*-
"""
Created on Sat Mar 27 22:16:20 2021

@author: mikyb
"""
import mysql.connector
import pandas as pd
import matplotlib.pyplot as plt 


class Plotting_Data():
    def __init__(self,table1,table2,table3,table4,table5,table6,answer):
        self.table1 = table1
        self.table2 = table2
        self.table3 = table3
        self.table4 = table4
        self.table5 = table5
        self.table6 = table6
        self.answer = answer
        self.db ='covid_corona_db_WHIT_BARI' 

    def connect_db(self):
        my_db = mysql.connector.connect(
                    host="localhost",
                    user="root",
                    password="Darling2b88!",
                    database=self.db)
        return my_db
    
    
    def panda_6_tables(self):     
        connec = self.connect_db()
        frame = pd.read_sql("SELECT NewCases,NewDeaths,NewRecovered, dates  \
                            FROM covid_corona_db_whit_bari."+self.table1+" \
                            WHERE Country_Other='"+self.answer+"' \
                            UNION \
                            SELECT NewCases,NewDeaths,NewRecovered, dates FROM covid_corona_db_whit_bari."+self.table2+" \
                            WHERE Country_Other='"+self.answer+"' \
                            UNION \
                            SELECT NewCases,NewDeaths,NewRecovered, dates FROM covid_corona_db_whit_bari."+self.table3+" \
                            WHERE Country_Other='"+self.answer+"' \
                            UNION \
                            SELECT NewCases,NewDeaths,NewRecovered, dates FROM covid_corona_db_whit_bari."+self.table4+" \
                            WHERE Country_Other='"+self.answer+"' \
                            UNION \
                            SELECT NewCases,NewDeaths,NewRecovered, dates FROM covid_corona_db_whit_bari."+self.table5+" \
                            WHERE Country_Other='"+self.answer+"' \
                            UNION \
                            SELECT NewCases,NewDeaths,NewRecovered, dates FROM covid_corona_db_whit_bari."+self.table6+" \
                            WHERE Country_Other='"+self.answer+"'",connec) 
                                
        return frame
    
    
    def panda_6_border(self):
        connec = self.connect_db()
        neighbours = self.top1neighbour('country_borders_table')
        string1= ''.join(neighbours[0])
        frame = pd.read_sql("SELECT country_other,neighbour,Distance,NewCases,dates \
                            FROM covid_corona_db_whit_bari."+self.table1+" \
                            INNER JOIN covid_corona_db_whit_bari.country_borders_table \
                            USING(country_other) \
                            WHERE Country_other IN ('"+self.answer+"','"+string1+"') \
                            AND Distance = (SELECT MAX(DISTANCE) \
                            FROM covid_corona_db_whit_bari.country_borders_table \
                            WHERE Country_other ='"+self.answer+"') \
                            UNION \
                            SELECT country_other,neighbour,Distance,NewCases,dates \
                            FROM covid_corona_db_whit_bari."+self.table2+" \
                            INNER JOIN covid_corona_db_whit_bari.country_borders_table  \
                            USING(country_other) \
                            WHERE Country_other IN ('"+self.answer+"','"+string1+"') \
                            AND Distance = (SELECT MAX(DISTANCE)  \
                            FROM covid_corona_db_whit_bari.country_borders_table \
                            WHERE Country_other ='"+self.answer+"') \
                            UNION \
                            SELECT country_other,neighbour,Distance,NewCases,dates \
                            FROM covid_corona_db_whit_bari."+self.table3+" \
                            INNER JOIN covid_corona_db_whit_bari.country_borders_table \
                            USING(country_other) \
                            WHERE Country_other IN ('"+self.answer+"','"+string1+"') \
                            AND Distance = (SELECT MAX(DISTANCE) \
                            FROM covid_corona_db_whit_bari.country_borders_table \
                            WHERE Country_other ='"+self.answer+"') \
                            UNION \
                            SELECT country_other,neighbour,Distance,NewCases,dates \
                            FROM covid_corona_db_whit_bari."+self.table4+" \
                            INNER JOIN covid_corona_db_whit_bari.country_borders_table \
                            USING(country_other) \
                            WHERE Country_other IN ('"+self.answer+"','"+string1+"') \
                            AND Distance = (SELECT MAX(DISTANCE) \
                            FROM covid_corona_db_whit_bari.country_borders_table \
                            WHERE Country_other ='"+self.answer+"') \
                            UNION \
                            SELECT country_other,neighbour,Distance,NewCases,dates \
                            FROM covid_corona_db_whit_bari."+self.table5+" \
                            INNER JOIN covid_corona_db_whit_bari.country_borders_table \
                            USING(country_other) \
                            WHERE Country_other IN ('"+self.answer+"','"+string1+"') \
                            AND Distance = (SELECT MAX(DISTANCE) \
                            FROM covid_corona_db_whit_bari.country_borders_table \
                            WHERE Country_other ='"+self.answer+"') \
                            UNION  \
                            SELECT country_other,neighbour,Distance,NewCases,dates \
                            FROM covid_corona_db_whit_bari."+self.table6+" \
                            INNER JOIN covid_corona_db_whit_bari.country_borders_table \
                            USING(country_other) \
                            WHERE Country_other IN ('"+self.answer+"','"+string1+"')  \
                            AND Distance = (SELECT MAX(DISTANCE) \
                            FROM covid_corona_db_whit_bari.country_borders_table \
                            WHERE Country_other ='"+self.answer+"')",connec)
        
        return frame
    
    
    def panda_3_border(self):
        connec = self.connect_db()
        neighbours = self.top2neighbours('country_borders_table')
        string1= ''.join(neighbours[0])
        string2= ''.join(neighbours[1])
        frame = pd.read_sql("SELECT DISTINCT(country_other),`Deaths/1M`  ,dates \
                            FROM covid_corona_db_whit_bari."+self.table1+" \
                            JOIN (covid_corona_db_whit_bari.country_borders_table) \
                            USING (COUNTRY_Other) \
                            WHERE country_other IN ('"+self.answer+"','"+string1+"','"+string2+"') \
                            UNION \
                            SELECT DISTINCT(country_other),`Deaths/1M`,dates  \
                            FROM covid_corona_db_whit_bari."+self.table2+" \
                            JOIN (covid_corona_db_whit_bari.country_borders_table) \
                            USING (COUNTRY_Other) \
                            WHERE country_other IN ('"+self.answer+"','"+string1+"','"+string2+"') \
                            UNION \
                            SELECT DISTINCT(country_other),`Deaths/1M`  ,dates \
                            FROM covid_corona_db_whit_bari."+self.table3+" \
                            JOIN (covid_corona_db_whit_bari.country_borders_table) \
                            USING (COUNTRY_Other)  \
                            WHERE country_other IN ('"+self.answer+"','"+string1+"','"+string2+"')",connec)
        
        return frame    
    
    
    def top2neighbours(self,table_name):
        connec = self.connect_db()
        mycursor = connec.cursor()
        query1="SELECT NEIGHBOUR FROM "+table_name+" \
                where country_other = '"+self.answer+"'  \
                ORDER BY Distance DESC \
                LIMIT 2" 
        mycursor.execute(query1)
        result = list(mycursor.fetchall())
        return result
    
    def top1neighbour(self,table_name):
        connec = self.connect_db()
        mycursor = connec.cursor()
        query1="SELECT NEIGHBOUR FROM "+table_name+" \
                where country_other = '"+self.answer+"'  \
                ORDER BY Distance DESC \
                LIMIT 1" 
        mycursor.execute(query1)
        result = list(mycursor.fetchall())
        return result
        
 
    def plot_1(self):
        #Bar Graphs for chosen country (COMPLETE)
        days = self.panda_6_tables()
        
        plotdata = pd.DataFrame(days,columns=['dates','country_other','NewCases','NewDeaths','NewRecovered'])
        
        print(plotdata.pivot(index='dates',columns='country_other',values=['NewCases','NewDeaths','NewRecovered']).plot(kind='bar'))
        plt.ylabel('3 Main Indicators')
        plt.title('6-days Key indicators evolution-'+self.answer)
        plt.show()
        
    def plot_2(self):
        #Bar Graphs for NewCases (COMPLETE)
        border = self.panda_6_border()
        plotdata = pd.DataFrame(border,columns=['dates','country_other','NewCases'])
        print(plotdata.pivot(index='dates',columns='country_other',values='NewCases').plot(kind='bar'))
        plt.ylabel('NewCases')
        plt.title('6days NewCases comparison -'+self.answer+' with longest distance neighbour ')
        plt.show()
    
    def plot_3(self):
        #Bar Graphs for Deaths/1M (COMPLETE)
        threeday = self.panda_3_border()        
              
        plt.figure()
        plotdata = pd.DataFrame(threeday,columns=['dates','country_other','Deaths/1M'])
        print(plotdata.pivot(index='dates',columns='country_other',values='Deaths/1M').plot(kind='bar',rot=0))
        plt.xlabel('Dates')
        plt.ylabel('Deaths/1M')
        plt.title('3days Deaths/1M pop comparison-'+self.answer+' with 2 neighbours')
