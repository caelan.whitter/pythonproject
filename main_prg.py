# -*- coding: utf-8 -*-
"""
Created on Mon Mar 29 11:27:23 2021

@author: Caelan
"""
from datetime import datetime
from data.cleandata import CleanData
from data.scrapping import *
from data.scrapping_json import *
from data.scrapping import *
from data.Database_management import *
from data.data_analyse_explore import Plotting_Data
import glob


def setting_table_name(url):
    url_split = (url.split('local_html/'))
    url_split2 = url_split[1].split('page')
    url_split3 =url_split2[1].split('.html')
    url_split4 = url_split3[0].split('-')
    table_name = 't'+url_split4[0]+'_'+url_split4[1]+'_'+url_split4[2]
    return table_name

def create_new_thing(file_one,file_two):
    file_name_split = file_one.split('\\')
    file_name = file_name_split[1]
    file_name_split2 = file_two.split('\\')
    file_name2 = file_name_split2[1]
    url='file:///C:/Users/Caelan/Documents/fourthsemester/python/pythonproject/local_html/'
    url1 = url+file_name
    url2 = url+file_name2

    table_name1= setting_table_name(url1)
    table_name2 = setting_table_name(url2)
    
    url_split = (url1.split('local_html/'))
    url_split2 = url_split[1].split('page')
    add_date_split =url_split2[1].split('.html')
    add_date = add_date_split[0]
    
    url_split = (url2.split('local_html/'))
    url_split2 = url_split[1].split('page')
    add_date2_split =url_split2[1].split('.html')
    add_date2 = add_date2_split[0]
    
    list_first_three = creating_new_table(url1,table_name1,add_date)

    list_second_three = creating_new_table(url2,table_name2,add_date2)

    

    answer = input('Enter a country to analyse  ')

    new_plots = Plotting_Data(list_first_three[0],list_first_three[1],list_first_three[2],
                          list_second_three[0],list_second_three[1],list_second_three[2],
                          answer)
    new_plots.plot_1()
    new_plots.plot_2()
    new_plots.plot_3()
    
    
    
    

create_db_tables_six_days()
adding_date_old_tables()
create_border_table()

print("1: save to local web file")
print("2: scrape from a saved local file")

choice = int(input("Enter your choice: "))

if choice == 1:
    url ='https://www.worldometers.info/coronavirus/'
    date = datetime.today().strftime('%Y-%m-%d')
    filename = 'local_page'+date+'.html'
    html_bytes = get_html(url)
    save_to_local_file(filename,html_bytes)
    print('file has been saved as '+filename)
    
    

if choice == 2:
    date = input("Enter YYYY-MM-DD: ")
    
    file_list = glob.glob('local_html/*.html')
    i=0
    file_date=''
    file_one=''
    file_two=''
    for files in file_list:
        file_split = files.split('page')
        file_split2 = file_split[1].split('.html')
        file_date = file_split2[0]
        if date == file_date:
            file_one=files
    date_split= date.split('-')
    date1 = date_split[0]+'-'+date_split[1]+'-'+str(int(date_split[2])+3)
    date2 = date_split[0]+'-'+date_split[1]+'-'+str(int(date_split[2])-3)

    for files in file_list:
        file_split = files.split('page')
        file_split2 = file_split[1].split('.html')
        file_date = file_split2[0]
        if date1 == file_date or date2 == file_date:
            file_two=files
    if len(file_one)==0 or len(file_two)==0:
        print('not enough files to make up 6-day analysis')
    else:
        print(file_one)
        print(file_two)
        create_new_thing(file_one,file_two)
        


