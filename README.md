# pythonproject

Caelan Whitter / 1841768
Mikael Baril / 1844064

things to be changed when run on another computer:

1- database logins in : Database_management.py, data_analysis_explore.py
2-file links : Database_management.py, scrapping_json.py,main_prg.py

how it works:
When main_prg.py is run, the six tables are created in the database along with the border table. 
When choosing choice 1, a file is automatically saved with the date.
When choosing choice 2, our program is made to only plot if there is enough data in a span of 6 days.
if you choose a file that has 6 days of data, it'll go through the plotting process and plot the plots.
